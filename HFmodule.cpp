#include "integrals.hpp"


void ReadGuessDensity(std::string filename, int nao,  Matrix& xD){

  std::ifstream myfile (filename);
  std::string line;
  std::string::size_type sz;  
  if (myfile.is_open())
  {
            for(int x = 0; x < nao; x++){
                for(int y = 0; y < nao; y++){
                    
                    if (std::getline(myfile, line)){
                        xD(x,y) = std::stod(line, &sz);
                        //std::cout << line << '\n';
                    }
                    else{
                        myfile.close();
                        return;
                    }
    
                }
            }
    myfile.close();
  }
}


Matrix HartreeFock(const Matrix& S, const Matrix& T, const Matrix& V,
        const Matrix& VV, const int nocc, const std::vector<Atom>& atoms,
        const std::string initial_D) {

    auto enuc = NuclearRepulsion(atoms);
    std::cout.precision(15);
    std::cout << "\n\tNuclear repulsion energy = " << enuc << std::endl;

    //Initial Density matrix
    Matrix D = Matrix::Zero(S.rows(),S.cols());
    Matrix C;
    Matrix C_occ;
    if (initial_D.compare("guess.rdm") == 0 ){
        ReadGuessDensity(initial_D, D.rows(), D); 

    } else {
        Eigen::GeneralizedSelfAdjointEigenSolver<Matrix> diag( T + V, S);
        auto eps = diag.eigenvalues();
        C = diag.eigenvectors();
        C_occ = C.leftCols(nocc);
        D = 2 * (C_occ*C_occ.transpose());
    }
   

    //iterate until self consistency
    const auto maxiter = 1000;                                          
    const auto conv = 1e-8;                                            
    auto iter = 0;                                                     
    auto rms = 0.0;                                                    
    auto ehf = 0.0;  
    int DIIS = 1; // DIIS=1 (on) DIIS=2 (off)
    int DIIS_E_len = 10; //number of vectors to include in the DIIS system
    Matrix G;
    auto H = T + V;
    Matrix F = H;

    //DIIS error and fock vectors
    std::deque<Matrix> error;  //use deque because access data without iterator
    std::deque<Matrix> fock_q; 

    

    auto D_old = D;
    do {                                                               
      const auto tstart = std::chrono::high_resolution_clock::now();   

      G = ConstructG(D, VV);
      F = H + G;

      if ( DIIS == 1 ) {

          if ( iter > 0 ){
              if ( error.size() < DIIS_E_len ){
                  error.push_back(F*D*S - S*D*F);
                  fock_q.push_back( F );
              } 
              if ( error.size() >= DIIS_E_len ) {
                  error.pop_front();
                  fock_q.pop_front();
                  error.push_back(F*D*S - S*D*F);
                  fock_q.push_back( F );
              }
          }

          if (error.size() >= 2) {
    
              //initialize B matrix
              Matrix B = Matrix::Zero(error.size() + 1, error.size() + 1);
              Eigen::VectorXd b = Eigen::VectorXd::Zero(B.rows());
              for(int ii = 0; ii < error.size(); ii ++) { B(0,ii+1) = -1; B(ii+1,0) = -1; }
              b(0) = -1.0;
              for(int erri = 0; erri < error.size(); erri++){
                  for(int errj = 0; errj < error.size(); errj++){
                    
                      B(erri + 1, errj + 1) = (error[erri]*(error[errj].transpose()) ).trace();
                  }
              }

              Eigen::VectorXd coeffs = B.fullPivHouseholderQr().solve(b);
              F = Matrix::Zero(G.rows(), G.cols());
              for(int ii = 0; ii < fock_q.size(); ii++){
                  F += coeffs(ii+1)*fock_q[ii];
              }
            
          }

      } 

      Eigen::GeneralizedSelfAdjointEigenSolver<Matrix> diag1( F, S);
      auto eps = diag1.eigenvalues();
      C = diag1.eigenvectors();
      C_occ = C.leftCols(nocc);
      D = 2 * ( C_occ * C_occ.transpose() );
      ehf = (0.5*D).cwiseProduct(2*(T + V) + G).sum();
      rms = (D - D_old).norm();

      if (DIIS != 1 ){
        D = 0.5*D + (1.0 - 0.5)*D_old;
      }
      const auto tstop = std::chrono::high_resolution_clock::now();
      const std::chrono::duration<double> time_elapsed = tstop - tstart;


      if (iter == 0) {
        std::cout << "\n\n Iter         (E(tot))          RMS(D)           Time(s)      Norm\n";
       }
      printf(" %02d  %20.12f %20.12e %10.5lf %10.5f\n", iter, ehf + enuc ,
              rms, time_elapsed.count(), (D*S).trace());     

    D_old = D;
    ++iter;                                                            
    } while  ( (fabs(rms) > conv) && (iter < maxiter));      
    std::cout << "FINAL ENERGY = " << ehf + enuc << std::endl;

    
    //Write final density matrix to guess.rdm
    std::ofstream ofs;
    ofs.precision(15);
    ofs.open ("guess.rdm", std::ofstream::out );
    for (int x = 0; x < D.rows(); x++){
        for(int y = 0; y < D.cols(); y++){
            ofs << D(x,y) << "\n";
        }
    }
    ofs.close();


    return C;

}

double NuclearRepulsion(const std::vector<Atom>& atoms) {

    double enuc = 0.0;
    for (auto i = 0; i < atoms.size(); i++){
      for (auto j = i+1; j < atoms.size(); j++) {

        auto xij = atoms[i].x - atoms[j].x;
        auto yij = atoms[i].y - atoms[j].y;
        auto zij = atoms[i].z - atoms[j].z;
        auto r2 = xij*xij + yij*yij + zij*zij;
        auto r = sqrt(r2);
        enuc += atoms[i].atomic_number * atoms[j].atomic_number / r;
     }
    }
    return enuc;
}
//Construct G-mat
Matrix ConstructG(const Matrix& D, const Matrix& VVee){

    /***********************************
     *
     *          Construct G-matrix
     *
     *
     * ********************************/
   
    int L = D.rows();
    Matrix G = Matrix::Zero(D.rows(), D.cols());
    for(int ii = 0; ii < D.rows(); ii++){
        for(int jj = 0; jj < D.cols(); jj++){

            for(int rr = 0; rr < L; rr++){
                for(int ss = 0; ss < L; ss++){
                    G(ii,rr) += D(ss,jj)*(VVee(ii*L + jj, rr*L + ss) - 
                            0.5*VVee(ii*L + jj, ss*L + rr) );
                }
            }

        }
    }

    return G;



}


