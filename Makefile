#MAKEFILE FOR PBC HARTREE FOCK USING LIBINT FOR INTEGRALS

EIGENDIR = /home/nick/opt/eigen-eigen-1306d75b4a21/
LIBINTDIR = /home/nick/opt/libint2.1.0.b2/

LIBINT_LINK = -std=c++11 -isystem ${LIBINTDIR}/include -isystem ${LIBINTDIR}/include/libint2 -L${LIBINTDIR}/lib -DSRCDATADIR=\"${LIBINTDIR}/share/libint/2.1.0-beta2/basis\"

# include headers the object include directory
CPPFLAGS += -Wall -Wno-sign-compare -std=c++11 -march=native -ffast-math -O3 -I./  -isystem ${EIGENDIR}/ ${LIBINT_LINK}
CXXPOSTFLAGS = -lint2


#LIBINT calls
CXX = g++
EXEC = integrals
CXXTESTSRC = $(EXEC).cpp HFmodule.cpp
CXXTESTOBJ = $(CXXTESTSRC:%.cpp=%.o)


$(EXEC): $(CXXTESTOBJ) 
	$(CXX) ${CPPFLAGS}  -o $@  $^ ${CXXPOSTFLAGS}

clean::
	-rm -rf $(TEST) *.o *.d

distclean:: realclean

realclean:: clean

targetclean:: clean
