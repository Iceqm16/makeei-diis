makeEI-DIIS
------
Computes the one- and two-electron integrals for a given system using
libint2.10b and run Hartree-Fock to get a set of molecular orbitals.   Program
returns one- and two-electron molecular orbital integrals as an EI file for
makeSDP.

input file should be .xyz extension

line 1: # of atoms
line 2: skip
line 3-N: atom name, x-coord, y-coord, z-coord
line N+1: skip
line N+2: basis set name

Dependencies

Needs libint and Eigen matrix library. 

Eigen is a collection of Header files that can be found @
[http://bitbucket.org/eigen/eigen/get/3.2.4.tar.gz](http://bitbucket.org/eigen/eigen/get/3.2.4.tar.gz)

Libint

[https://github.com/evaleev/libint](https://github.com/evaleev/libint)
