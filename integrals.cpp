#if __cplusplus <= 199711L
# error "Program requires C++11 support"
#endif

// integrals include
#include "integrals.hpp"

#if defined(_OPENMP) 
# include <omp.h>
#endif

//extern std::string basis_set;
std::string basis_set = "sto-3g";
std::string initial_D = "CORE";


template <typename T> 
void printMats(std::vector<T>& mats){
    
    for(auto i(0); i < mats.size(); i++){
        std::cout << mats[i] << "\n" << std::endl;
    }
}

Matrix Kronecker(Matrix& mat1, Matrix& mat2){

    Matrix m3( mat1.rows()*mat2.rows(), mat1.cols()*mat2.cols());
    m3.setZero();

    for(int i = 0; i < mat1.cols(); i++){
        for(int j = 0; j < mat1.rows(); j++){

            m3.block(i*mat2.rows(),  j*mat2.cols(), mat2.rows(), mat2.cols()) = mat1(i,j)*mat2;
        }
    }

    return m3;
}

Eigen::MatrixXcd Kronecker(Eigen::MatrixXcd& mat1, Eigen::MatrixXcd& mat2){

    Eigen::MatrixXcd m3( mat1.rows()*mat2.rows(), mat1.cols()*mat2.cols());
    m3.setZero();

    for(int i = 0; i < mat1.cols(); i++){
        for(int j = 0; j < mat1.rows(); j++){

            m3.block(i*mat2.rows(),  j*mat2.cols(), mat2.rows(), mat2.cols()) = mat1(i,j)*mat2;
        }
    }

    return m3;
}


int main(int argc, char *argv[]) {

  using std::cout;
  using std::cerr;
  using std::endl;

  Eigen::IOFormat HeavyFmt(15, 0, ", ", ",\n", "[", "]", "[", "]");
  // initializes the Libint integrals library ... now ready to compute
  libint2::init();

  try {

    /*** =========================== ***/
    /*** initialize molecule         ***/
    /*** =========================== ***/

    const auto filename = (argc > 1) ? argv[1] : "h2o.xyz";
    std::vector<Atom> atoms = read_geometry(filename);
    BasisSet obs(basis_set, atoms);
    cout << "\tbasis set = " << basis_set << endl;
    cout << "\tbasis rank = " << obs.nbf() << endl;
    cout << "\tInitial Density Matrix = " << initial_D << endl;
 
    size_t nao = 0;
    for (auto s=0; s<obs.size(); ++s)
      nao += obs[s].size();

    int nocc = 0;
    for(auto i = 0; i < atoms.size(); i++){
        nocc += atoms[i].atomic_number;
    }
    nocc = 0.5*nocc;


    /*** =========================== ***/
    /*** compute 1-e integrals       ***/
    /*** =========================== ***/


    
    printf("\n\tATOMS\n");
    printf("\tX\tY\tZ\tZ\n");
    for(auto i = 0; i < atoms.size(); i++){
        printf("\t%f\t%f\t%f\t%d\n", atoms[i].x, atoms[i].y, atoms[i].z, 
                atoms[i].atomic_number);
    }

   
    
    Matrix S = compute_1body_ints<libint2::OneBodyEngine::overlap>(obs )[0];
    Matrix T = compute_1body_ints<libint2::OneBodyEngine::kinetic>(obs )[0];
    Matrix V = compute_1body_ints<libint2::OneBodyEngine::nuclear>(obs,  atoms)[0];

    //cout << "Overlaps\n";
    //for(int i = 0; i < nao; i++){
    //  for(int j = i; j < nao; j++){
    //      printf("<%i,%i> = %20.15f\n",j,i,S(i,j));
    //  }
    //}
    //cout << "Kinetic\n";
    //for(int i = 0; i < nao; i++){
    //  for(int j = i; j < nao; j++){
    //      printf("<%i,%i> = %20.15f\n",j,i,T(i,j));
    //  }
    //}
    //cout << "Potential\n";
    //for(int i = 0; i < nao; i++){
    //  for(int j = i; j < nao; j++){
    //      printf("<%i,%i> = %20.15f\n",j,i,V(i,j));
    //  }
    //}

    const auto tstart = std::chrono::high_resolution_clock::now();
    Matrix VV = compute_2body_ints(obs, nao, atoms);
    const auto tstop = std::chrono::high_resolution_clock::now();
    const std::chrono::duration<double> time_elapsed = tstop - tstart;
 
    printf("\ttotal 2-el int time (master) = %f\n", time_elapsed.count());

    cout << "Two-Electron Integrals\n" ;
    cout << "V2 size " << VV.rows() <<  "," << VV.cols() << endl;
    int twoEl_count = 0;
    for(int i = 0; i < nao; i++){
      for(int j = 0; j < nao; j++){
        for(int k = 0; k < nao; k++){
          for(int l = 0; l < nao; l++){

            //printf("<%i,%i|%i,%i> = %20.15f\n", i,j, k,l,VV(i*nao + j, k*nao + l));
            if (fabs(VV(i*nao + j, k*nao +l)) > double(1.0E-10) ) {
                twoEl_count++;
            }

          }
        }
      }
    }

    libint2::cleanup(); // done with libint

    Matrix Coeffs = HartreeFock(S, T, V, VV, nocc , atoms, initial_D);
   
    //rotate 1-electron integrals
    Matrix H1MO = Matrix::Zero(nao,nao);
    Matrix Core = T + V;
    //std::cout << Core << std::endl;
    H1MO = ( Coeffs.transpose()*Core)*Coeffs; 
    
    Matrix SCoeffs = Kronecker(Coeffs, Coeffs);
    Matrix VVMO = SCoeffs.transpose()*(VV*SCoeffs); 

    twoEl_count = 0;
    for(int i = 0; i < nao; i++){
      for(int j = 0; j < nao; j++){
        for(int k = 0; k < nao; k++){
          for(int l = 0; l < nao; l++){

            //printf("<%i,%i|%i,%i> = %20.15f\n", i,j, k,l,VVMO(i*nao + j, k*nao + l));
            if (fabs(VVMO(i*nao + j, k*nao +l)) > double(1.0E-10) ) {
                twoEl_count++;
            }

          }
        }
      }
    }



    //DO Hartree-Fock...rotate vectors...print Hamiltonian to file
    //write EI file
    std::cout << "writing new EI file \n";
    std::cout.precision(15);
    std::ofstream ofs;
    ofs.precision(15);
    ofs.open ("ncrEI.ei", std::ofstream::out );
    ofs << nocc << "\t" << nao << "\t1.0e-5\t" << twoEl_count  << "\t0\n";
    for(int i = 0; i < nao; i++){
        for(int j = i; j < nao; j++){
            ofs << i+1 << "\t" << j+1 << "\t0\t0\t" << H1MO(i,j) << "\n";
        }
    }
    for(int i = 0; i < nao; i++){
      for(int j = 0; j < nao; j++){
        for(int k = 0; k < nao; k++){
          for(int l = 0; l < nao; l++){

            if (fabs(VVMO(i*nao + j, k*nao +l)) > double(1.0E-10) ) {
                ofs << i+1 << "\t" << j+1 << "\t" << k+1 << "\t" << l+1 << "\t"
                    << VVMO(i*nao + j, k*nao +l) << "\n";
            }

          }
        }
      }
    }

   

    ofs.close();
    
    
    
  } // end of try block; if any exceptions occurred, report them and exit cleanly

  catch (const char* ex) {
    cerr << "caught exception: " << ex << endl;
    return 1;
  }
  catch (std::string& ex) {
    cerr << "caught exception: " << ex << endl;
    return 1;
  }
  catch (std::exception& ex) {
    cerr << ex.what() << endl;
    return 1;
  }
  catch (...) {
    cerr << "caught unknown exception\n";
    return 1;
  }

  return 0;



}



std::vector<Atom> read_geometry(const std::string& filename) {

  std::cout << "\tWill read geometry from " << filename << std::endl;
  std::ifstream is(filename);
  assert(is.good());

  // to prepare for MPI parallelization, we will read the entire file into a string that can be
  // broadcast to everyone, then converted to an std::istringstream object that can be used just like std::ifstream
  std::ostringstream oss;
  oss << is.rdbuf();
  // use ss.str() to get the entire contents of the file as an std::string
  // broadcast
  // then make an std::istringstream in each process
  std::istringstream iss(oss.str());

  // check the extension: if .xyz, assume the standard XYZ format, otherwise throw an exception
  if ( filename.rfind(".xyz") != std::string::npos){
    std::vector<Atom> atoms = libint2::read_dotxyz(iss);

    //read basis set from file
    std::string basis_tmp;
    iss >> basis_tmp;
    basis_set.assign(basis_tmp);
    //read in initial Density
    std::string initial_D_tmp;  
    iss >> initial_D_tmp;
    if (initial_D_tmp.length() > 0){
        initial_D.assign(initial_D_tmp);
    }

    return atoms;
  }
  else {
    throw "only .xyz files are accepted";
  }

}


template <libint2::OneBodyEngine::operator_type obtype>
std::array<Matrix, libint2::OneBodyEngine::operator_traits<obtype>::nopers>
compute_1body_ints(BasisSet& obs,
                          const std::vector<Atom>& atoms){

  
  const auto n = obs.nbf();

#ifdef _OPENMP
  const auto nthreads = 1;//Set to 1 regardless//omp_get_max_threads();
#else
  const auto nthreads = 1;
#endif
  typedef std::array<Matrix, libint2::OneBodyEngine::operator_traits<obtype>::nopers> result_type;
  const unsigned int nopers = libint2::OneBodyEngine::operator_traits<obtype>::nopers;
  result_type result; for(auto& r: result) r = Matrix::Zero(n,n);


  std::vector<libint2::OneBodyEngine> engines(nthreads); //one thread
  engines[0] = libint2::OneBodyEngine(obtype, obs.max_nprim(), obs.max_l(), 0);

  if (obtype == libint2::OneBodyEngine::nuclear) {

    std::vector<std::pair<double,std::array<double,3>>> q;
        for(const auto& atom : atoms) {
        q.push_back( {static_cast<double>(atom.atomic_number), 
                {{atom.x, atom.y, atom.z}}} );
        }

    engines[0].set_params(q);
  }


  auto shell2bf = obs.shell2bf();
  auto shell2bf_shift = obs.shell2bf();


  for(auto s1=0; s1!=obs.size(); ++s1) {

    auto bf1 = shell2bf[s1]; // first basis function in this shell
    auto n1 = obs[s1].size();

    for(auto s2=0; s2!=obs.size(); ++s2) {

      auto bf2 = shell2bf[s2];
      auto n2 = obs[s2].size();
      
      auto n12 = n1 * n2;
    
      const auto* buf = engines[0].compute( obs[s1], obs[s2]);

      for(unsigned int op=0; op!=nopers; ++op, buf+=n12) {
        Eigen::Map<const Matrix> buf_mat(buf, n1, n2);
        result[op].block(bf1, bf2, n1, n2) = buf_mat;
      }
    }
  }

  return result;
}


Matrix compute_2body_ints(BasisSet& shells, const int nao, std::vector<Atom>& atoms){


  const auto n = shells.nbf();
  Matrix VV  = Matrix::Zero(nao*nao, nao*nao);


// construct the electron repulsion integrals engine
  typedef libint2::TwoBodyEngine<libint2::Coulomb> coulomb_engine_type;
  std::vector<coulomb_engine_type> engines(1); //1 thread
  engines[0] = coulomb_engine_type(shells.max_nprim(), shells.max_l(), 0);
  

  engines[0].set_precision(std::numeric_limits<double>::epsilon());

  auto shell2bf = shells.shell2bf();

  //shell integrals look like (ab|cd) where ab correspond to distribution for
  //electron 1 and cd correspond to distriution for electrion 2.  To put into
  //physics notation we must do <a,d|b,c>
  for(auto s1=0; s1!=shells.size(); ++s1) {

    auto bf1_first = shell2bf[s1]; // first basis function in this shell
    auto n1 = shells[s1].size();

    for(auto s2=0; s2!=shells.size(); ++s2) {


      auto bf2_first = shell2bf[s2];
      auto n2 = shells[s2].size();

      for(auto s3=0; s3!=shells.size(); ++s3) {

        auto bf3_first = shell2bf[s3];
        auto n3 = shells[s3].size();

        for(auto s4=0; s4!=shells.size(); ++s4) {

          auto bf4_first = shell2bf[s4];
          auto n4 = shells[s4].size();


          const auto* buf_1234 = engines[0].compute(shells[s1], shells[s2], shells[s3], shells[s4] );


        //loop over all integrals in shell set and extract 
          for(auto f1=0, f1234=0; f1!=n1; ++f1) {
            const auto bf1 = f1 + bf1_first;
            for(auto f2=0; f2!=n2; ++f2) {
              const auto bf2 = f2 + bf2_first;
              for(auto f3=0; f3!=n3; ++f3) {
                const auto bf3 = f3 + bf3_first;
                for(auto f4=0; f4!=n4; ++f4, ++f1234) {
                  const auto bf4 = f4 + bf4_first;

                   //store in physics notation 
                  VV(bf1*n+bf3, bf2*n+bf4) = buf_1234[f1234];
                  //printf("<%i,%i|%i,%i>\n",int(bf1),int(bf3),int(bf2),int(bf4));


                }
              }
            }
          }



        }
      }
    }
  }


  return VV;

}


