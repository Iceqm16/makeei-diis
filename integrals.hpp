/***
 * This is the header file for integrals.  
 *
 * */

#ifndef INTEGRALS_H
#define INTEGRALS_H

// standard C++ headers
#include <cmath>
#include <complex>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <deque>

// Eigen matrix algebra library
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>


// Libint Gaussian integrals library
#include <libint2.hpp>

using libint2::Shell;
using libint2::Atom;
using libint2::BasisSet;



typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
        Matrix;  
typedef Eigen::Matrix<std::complex<double>, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> CMatrix; 

std::vector<Atom> read_geometry(const std::string& filename);

template <libint2::OneBodyEngine::operator_type obtype>
std::array<Matrix, libint2::OneBodyEngine::operator_traits<obtype>::nopers>
compute_1body_ints( BasisSet& ,
                          const std::vector<Atom>& atoms = std::vector<Atom>()
                          );

//Matrix compute_1body_ints( BasisSet& ,
//                          libint2::OneBodyEngine::integral_type,
//                          const std::vector<Atom>& atoms = std::vector<Atom>()
//                          );

Matrix compute_2body_ints(BasisSet&, 
        const int, std::vector<Atom>&);

Matrix HartreeFock(const Matrix& ,const Matrix& ,const Matrix& ,const  Matrix&,
        const int, const std::vector<Atom>&,
        const std::string);
Matrix ConstructG(const Matrix& , const Matrix& );
double NuclearRepulsion(const std::vector<Atom>& );
void ReadGuessDensity(std::string , int,  Matrix& );
#endif /* integrals.h */
